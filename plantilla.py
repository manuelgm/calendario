#!/usr/bin/python3

calendar = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    """Devuelve todas las actividades para la hora atime como una lista de tuplas"""
    ...
    return activities

def get_all():
    """Devuelve todas las actividade sen el calendario como una lista de tuplas"""
    return all

def get_busiest():
    """Devuelve la fecha con más actividades, y su número de actividades"""
    return (busiest, busiest_no)

def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    return ...

def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    return ...

def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    return (date, time, activity)

def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

def run_option(option):
    """Ejecuta el código necesario para la opción dada"""

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()