#!/usr/bin/python3

calendar = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    activities = []
    for date, times in calendar.items():
        for t, activity in times.items():
            if t == atime:
                activities.append((date, t, activity))
    return activities

def get_all():
    all_activities = []
    for date, times in calendar.items():
        for time, activity in times.items():
            all_activities.append((date, time, activity))
    return all_activities

def get_busiest():
    busiest_date = max(calendar, key=lambda date: len(calendar[date]))
    busiest_no = len(calendar[busiest_date])
    return (busiest_date, busiest_no)

def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    return len(date) == 10 and date[4] == date[7] == '-' and date[:4].isdigit() and date[5:7].isdigit() and date[8:].isdigit()

def check_time(time):
    return len(time) == 5 and time[:2].isdigit() and time[2] == ':' and time[3:].isdigit()

def get_activity():
    while True:
        date = input("Fecha (aaaa-mm-dd): ")
        time = input("Hora (hh:mm): ")
        activity = input("Actividad: ")

        if check_date(date) and check_time(time):
            return date, time, activity
        else:
            print("Formato de fecha u hora incorrecto. Inténtalo de nuevo.")

def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

def run_option(option):
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
        print("Actividad añadida correctamente.\n")
    elif option == 'B':
        activities = get_all()
        show(activities)
    elif option == 'C':
        busiest_date, busiest_no = get_busiest()
        print(f"El día más ocupado es el {busiest_date}, con {busiest_no} actividad(es).\n")
    elif option == 'D':
        atime = input("Hora (hh:mm): ")
        activities = get_time(atime)
        show(activities)

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()
